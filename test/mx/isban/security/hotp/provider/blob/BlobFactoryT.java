/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.isban.security.hotp.provider.blob;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Properties;
import mx.isban.security.hotp.provider.blob.BlobIO;
import mx.isban.security.hotp.provider.blob.BlobIO;
import mx.isban.security.hotp.provider.blob.BlobStatus;
import mx.isban.security.hotp.provider.blob.BlobStatus;
import mx.isban.security.hotp.provider.blob.BlobStruct;
import mx.isban.security.hotp.provider.blob.BlobStruct;
import mx.isban.security.hotp.provider.exception.MissingRequiredParameters;

/**
 *
 * @author C019301
 */
public class BlobFactoryT {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, MissingRequiredParameters {
        // TODO code application logic here
        
        BlobStruct blob = new BlobStruct();
        blob.setAttempts(0);
        blob.setCounter(0);
        blob.setLastUse(System.currentTimeMillis());
        blob.setRandData("123".getBytes());
        blob.setSeed("1234".getBytes());
        blob.setStatus(BlobStatus.ACTIVE);
        blob.setUserId("albertEinstein");
        blob.setVersion(1);
        Properties config = new Properties();
        config.put("server", "local");
        config.put("channel", "enterprise");
        blob.setXtraConfig(config);
                
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        BlobIO bFc = new BlobIO();        
        bFc.saveData(bos, blob);        
        byte[] data = bos.toByteArray();
        bos.close();
        
        ByteArrayInputStream bis = new ByteArrayInputStream(data);
        BlobStruct blSt = bFc.loadData(bis);
        System.out.println(blSt.toString());
    }
    
}
