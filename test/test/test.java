package test;


import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import mx.isban.security.hotp.provider.HotpProvider;
import mx.isban.security.hotp.provider.RandomGenerator;
import mx.isban.security.hotp.provider.SeedBuilder;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author C019301
 */
public class test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws NoSuchAlgorithmException, InvalidKeyException, InvalidKeySpecException {
        // TODO code application logic here
        byte [] derivedKey = SeedBuilder.buildRandomSeed(new String[]{"admin123","password"} , 2048);
        System.out.println(derivedKey.length);
        long counter = 0;
        HotpProvider hotp = new HotpProvider();    
        long e = System.currentTimeMillis();
        for (int a=0;a<12;a++){
            System.out.println(hotp.generateOTP(derivedKey, a, 4));
        }
        long s = System.currentTimeMillis();
        System.out.println(s-e);
    }
    
}
